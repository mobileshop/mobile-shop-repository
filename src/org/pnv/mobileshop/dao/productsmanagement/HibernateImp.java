package org.pnv.mobileshop.dao.productsmanagement;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.pnv.mobileshop.model.Products;
import org.pnv.mobileshop.model.ProductsDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class HibernateImp implements Hibernate{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<ProductsDetail> selectAllProduct() {
		List<ProductsDetail> allProduct = sessionFactory.getCurrentSession().createQuery("from ProductsDetail").list();
		return allProduct;
	}

	@Override
	public String findProductById(int id) {
		String strQuery = "SELECT P.rearCamera from Products P WHERE products_Id = :productsID";
		Query query = sessionFactory.getCurrentSession().createQuery(strQuery);
        query.setParameter("productsID", id);
        
		return (String) query.uniqueResult();
	}
}
