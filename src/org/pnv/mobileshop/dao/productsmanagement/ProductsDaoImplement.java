package org.pnv.mobileshop.dao.productsmanagement;

import java.sql.*;

import org.pnv.mobileshop.model.Color;
import org.pnv.mobileshop.model.Products;
import org.pnv.mobileshop.model.ProductsDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import java.util.List;


@Service  
@Transactional
public class ProductsDaoImplement implements ProductsDao {
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	
	// DONE
	public List<ProductsDetail> selectPriceAndNameAndImageOfProduct() {
		List<ProductsDetail> allProduct = sessionFactory.getCurrentSession().createQuery("from ProductsDetail").list();
		return allProduct;
	}
	// DONE
	@Override
	public List<Products> selectAllInformationOfProduct() {
		List<Products> allProduct = sessionFactory.getCurrentSession().createQuery("from Products").list();
		return allProduct;
	}
	
	//DONE
	@Override
	public List<ProductsDetail> selectPriceAndNameAndImageOfProductByBrand(String brand) {
		String showInformationAboutBrandProductCustomerFind = "from ProductsDetail AS P WHERE P.products.brand LIKE :brand";
		Query query = sessionFactory.getCurrentSession().createQuery(showInformationAboutBrandProductCustomerFind);
		query.setParameter("brand", "%"+ brand + "%");
		return (List<ProductsDetail>) query.list();
	}
	// DONE
	@Override
	public List<ProductsDetail> selectPriceAndNameAndImageOfProductByName(String name) {
		String showInformationAboutNameProductCustomerFind = "SELECT Price, ProductsName, ImageUrl"
				+ " FROM Products AS p" + " INNER JOIN ProductsDetail AS pd" + " ON p.Products_Id = pd.Products_Id"
				+ " INNER JOIN Products_Image AS pi" + " ON pd.ProductsDetail_Id = pi.ProductsDetail_Id "
				+ "WHERE MATCH(p.productsName) AGAINST('" + ":name" + "')";
		Query query = sessionFactory.getCurrentSession().createQuery(showInformationAboutNameProductCustomerFind);
		query.setParameter("name", name);
		
		List<ProductsDetail> showProductByName = (List<ProductsDetail>) query.list();
		return showProductByName;
	}
	
	// DONE
	@Override
	public List<ProductsDetail> selectPriceAndNameAndImageOfProductsByOperationSystem(String operationSystem) {
		String showInformationAboutOperationSystemProductCustomerFind = "from ProductsDetail AS P WHERE P.products.operatingSystem LIKE :opera";  
		Query query = sessionFactory.getCurrentSession().createQuery(showInformationAboutOperationSystemProductCustomerFind);
		query.setParameter("opera","%"+ operationSystem + "%");
		return (List<ProductsDetail>) query.list();
	}
	
	// DONE
	@Override
	public ProductsDetail selectAllInformationProductsById(int id) {
		String HQL = "from ProductsDetail AS P WHERE P.products.products_Id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(HQL);
		query.setParameter("id", id);
		ProductsDetail productDetail = (ProductsDetail) query.uniqueResult();
		return productDetail;
	}
	

	/*	// select information by id
	@Override
	public ProductsDetail selectAllInformationProductsById(int productsId) {
		ProductsDetail productsDetail = null;
		String showInformationAboutIdProductCustomerFind = "SELECT p.*, Pi.ImageUrl, Pd.Price FROM Products AS p"
				+ " INNER JOIN ProductsDetail AS Pd" + " ON p.Products_Id = Pd.Products_Id"
				+ " INNER JOIN Products_Image AS Pi"
				+ " ON Pd.ProductsDetail_Id = Pi.ProductsDetail_Id WHERE p.Products_Id =" + productsId + "";
		try {
			connection = getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(showInformationAboutIdProductCustomerFind);

			while (resultSet.next() == true) {
				productsDetail = new ProductsDetail(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), resultSet.getString(7),
						resultSet.getString(8), resultSet.getString(9), resultSet.getDate(10), resultSet.getString(11),
						resultSet.getString(12), resultSet.getInt(13));
			}
			close(connection);
		} catch (Exception e) {
			System.out.println("can't show all information of products");
		}
		return productsDetail;
	}*/

	
	// DONE
	@Override
	public List<ProductsDetail> selectPriceAndNameAndImageOfProductsByOrderAscending() {
		String showInformationByOrderAscending = "from ProductsDetail AS P ORDER BY P.price ASC"; 
		Query query = sessionFactory.getCurrentSession().createQuery(showInformationByOrderAscending);
		
		List<ProductsDetail> showProductsByOrderAscending = (List<ProductsDetail>) query.list();
		return showProductsByOrderAscending;
	}
	
	// DONE
	@Override
	public List<ProductsDetail> selectPriceAndNameAndImageOfProductsByOrderDescending() {
		String showInformationByOrderDescending = "from ProductsDetail AS P ORDER BY P.price DESC";
		Query query = sessionFactory.getCurrentSession().createQuery(showInformationByOrderDescending);
		List<ProductsDetail> showProductsByOrderDescending = (List<ProductsDetail>) query.list();
		return showProductsByOrderDescending;
	}
	
	// DONE
	@Override
	public List<ProductsDetail> seclectPriceAndNameAndImageOfProductsByPrice(String firstBoundary,
			String lastBoundary) {
		int firstB = Integer.parseInt(firstBoundary);
		int lastB = Integer.parseInt(lastBoundary);
		
		String showInformationAboutBrandProductCustomerFind = "from ProductsDetail AS P  WHERE P.price > :firstBoundary AND P.price < :lastBoundary ";
		Query query = sessionFactory.getCurrentSession().createQuery(showInformationAboutBrandProductCustomerFind);
		query.setParameter("firstBoundary", firstB);
		query.setParameter("lastBoundary", lastB);
		
		List<ProductsDetail> showProductsByPrice = (List<ProductsDetail>) query.list();
		return showProductsByPrice;
	}
	
	@Override
	public List<ProductsDetail> selectColorForProductByProductId(int id) {
		String showColor = "from ProductsDetail WHERE products.products_Id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(showColor);
		query.setParameter("id", id);
		
		List<ProductsDetail> showProductsByColor = (List<ProductsDetail>) query.list();
		return showProductsByColor;
	}
	
/*	
	@Override
	public List<Color> selectColorForProductByProductId(int id) {
		List<Color> colorProduct = new ArrayList<Color>();
		String showColor = "SELECT pd.Color_Id, c.Color"
				+ " FROM Color AS c" + " INNER JOIN ProductsDetail AS pd" + " ON pd.Color_Id = c.Color_Id"
				+ " WHERE pd.Products_Id = " + id + "";
		try {
			connection = getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(showColor);
			while (resultSet.next() == true) {
				colorProduct.add(new Color(resultSet.getInt(1), resultSet.getString(2)));
			}
			close(connection);
		} catch (Exception e) {
			System.out.println("cann't query show pro for home page");
		}
		return colorProduct;
	}*/
	
	

	@Override
	public List<ProductsDetail> seclectPriceAndNameAndImageOfProductsBySearch(String SearchKeyWord) {

		String showInformationAboutSearchProductCustomerFind = "SELECT Price, ProductsName, ImageUrl"
				+ " FROM Products AS p" 
				+ " INNER JOIN ProductsDetail AS pd" 
				+ " ON p.Products_Id = pd.Products_Id"
				+ " INNER JOIN Products_Image AS pi" 
				+ " ON pd.ProductsDetail_Id = pi.ProductsDetail_Id "
				+ " WHERE p.ProductsName LIKE '%" +":SearchKeyWord" + "%'"
				+ " OR p.Brand LIKE '%" + ":SearchKeyWord" +"%'";
		Query query = sessionFactory.getCurrentSession().createQuery(showInformationAboutSearchProductCustomerFind);
		query.setParameter("SearchKeyWord", SearchKeyWord);
		
		List<ProductsDetail> showProductsBySearch = (List<ProductsDetail>) query.list();
		
		return showProductsBySearch;
	}

	/*
	
	@Override
	public List<ProductsDetail> seclectPriceAndNameAndImageOfProductsBySearch(String SearchKeyWord) {
		List<ProductsDetail> productsDetail = new ArrayList<ProductsDetail>();
		String showInformationAboutSearchProductCustomerFind = "SELECT Price, ProductsName, ImageUrl"
															+ " FROM Products AS p" 
															+ " INNER JOIN ProductsDetail AS pd" 
															+ " ON p.Products_Id = pd.Products_Id"
															+ " INNER JOIN Products_Image AS pi" 
															+ " ON pd.ProductsDetail_Id = pi.ProductsDetail_Id "
															+ " WHERE p.ProductsName LIKE '" +SearchKeyWord + "%'"
															+ " OR p.Brand LIKE '" + SearchKeyWord +"%'";
		try {
			
			connection = getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery(showInformationAboutSearchProductCustomerFind);
			int price;
			String productsName = null;
			String imageUrl = null;
			
			while (resultSet.next() == true) {
				price = resultSet.getInt(1);
				productsName = resultSet.getString(2);
				imageUrl = resultSet.getString(3);
				productsDetail.add(new ProductsDetail(price, productsName, imageUrl));
			}
			close(connection);
		} catch (Exception e) {
			System.out.println("can't query show products by Search");
		}
		return productsDetail;
	}
	
	public static void main(String args[]){
		ProductsDaoImplement a = new ProductsDaoImplement();
		List<ProductsDetail> productsDetail = a.seclectPriceAndNameAndImageOfProductsBySearch("samsung");
		System.out.println(productsDetail.size());
	}*/
}
