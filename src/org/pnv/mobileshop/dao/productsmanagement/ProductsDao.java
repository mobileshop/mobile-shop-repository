package org.pnv.mobileshop.dao.productsmanagement;

import org.pnv.mobileshop.model.*;

import java.util.*;

public interface ProductsDao {
	List<ProductsDetail> selectPriceAndNameAndImageOfProduct();

	List<Products> selectAllInformationOfProduct();

	List<ProductsDetail> selectPriceAndNameAndImageOfProductByBrand(String brand);

	List<ProductsDetail> selectPriceAndNameAndImageOfProductByName(String name);

	List<ProductsDetail> selectPriceAndNameAndImageOfProductsByOperationSystem(String operationSystem);

	ProductsDetail selectAllInformationProductsById(int id);
	
	List<ProductsDetail> selectPriceAndNameAndImageOfProductsByOrderAscending();
	
	List<ProductsDetail> selectPriceAndNameAndImageOfProductsByOrderDescending();
	
	List<ProductsDetail> seclectPriceAndNameAndImageOfProductsByPrice(String firstBoundary, String lastBoundary);
	
	List<ProductsDetail> selectColorForProductByProductId(int id);
	
	List<ProductsDetail> seclectPriceAndNameAndImageOfProductsBySearch(String SearchKeyWord);
}
