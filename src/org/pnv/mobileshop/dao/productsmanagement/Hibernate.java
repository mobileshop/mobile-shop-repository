package org.pnv.mobileshop.dao.productsmanagement;

import java.util.List;

import org.pnv.mobileshop.model.Products;
import org.pnv.mobileshop.model.ProductsDetail;

public interface Hibernate {
	List<ProductsDetail> selectAllProduct();
	String findProductById(int id);
}
