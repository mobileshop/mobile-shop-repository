package org.pnv.mobileshop.dao.comment;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import java.util.ArrayList;
import java.util.List;
import org.pnv.mobileshop.model.Comments;
import org.pnv.mobileshop.model.ProductsDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.sql.*;
import java.text.SimpleDateFormat;

@Service
@Transactional
public class CommentsDaoImplement implements CommentDao {

	@Autowired
	private SessionFactory sessionFactory;

	Date today;
	// convert day to string
	SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd");

	// DONE
	@Override
	public List<Comments> selectCommentFromCommentTableByProductId(int Products_Id) {
		String ListComment = "from Comments AS C WHERE C.products.products_Id = :id ";
		Query query = sessionFactory.getCurrentSession().createQuery(ListComment);
		query.setParameter("id", Products_Id);
		List<Comments> comments = (List<Comments>) query.list();
		return comments;
	}
	// DONE
	@Override
	public void insertCommentToCommentTable(Comments comment) {
		sessionFactory.getCurrentSession().save(comment);
	}
}