package org.pnv.mobileshop.dao.comment;

import java.util.List;
import org.pnv.mobileshop.model.Comments;

public interface CommentDao {
	List<Comments> selectCommentFromCommentTableByProductId(int Products_Id);
	void insertCommentToCommentTable(Comments comment);
}
