package org.pnv.mobileshop.dao.usermanagement;
import org.pnv.mobileshop.model.User;


import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class UserDaoImplement implements UserDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	// DONE
	@Override
	public User selectUserNameAndPasswordInUserTable(String userName, String passWord) {
		String StrQuery = "from User WHERE userName = :username AND passWord = :password";
		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
		query.setParameter("username", userName);
		query.setParameter("password", passWord);

		User user = (User) query.uniqueResult();
		if (user != null) {
			return user;
		}
		return null;
	}
	
	// DONE
	@Override
	public User insertNewDataIntoUserTable(User us) {
		sessionFactory.getCurrentSession().save(us);
		return new User();
	}
	// DONE
	@Override
	public boolean selectUserNameFromUserTable(String userName) {
		String StrQuery = "from User WHERE userName = :username";
		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
		query.setParameter("username", userName);
		User user = (User) query.uniqueResult();
		if (user == null) {
			return false;
		} else {
			return true;
		}
	}
	// DONE
	@Override
	public String selectFullNameByUserId(int user_id) {
		String StrQuery = "SELECT fullName FROM User WHERE user_Id = :user_Id";
		Query query = sessionFactory.getCurrentSession().createQuery(StrQuery);
		query.setParameter("user_Id",user_id);
		return (String) query.uniqueResult();
	}
}
