package org.pnv.mobileshop.dao.usermanagement;

import org.pnv.mobileshop.model.User;

public interface UserDao {
	User selectUserNameAndPasswordInUserTable(String userName, String passWord);
	User insertNewDataIntoUserTable(User us);
	boolean selectUserNameFromUserTable(String userName);
	public String selectFullNameByUserId(int user_id);
}
