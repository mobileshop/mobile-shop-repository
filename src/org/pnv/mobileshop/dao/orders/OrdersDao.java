package org.pnv.mobileshop.dao.orders;

import java.util.List;

import org.pnv.mobileshop.model.OrdersDetail;
import org.pnv.mobileshop.model.ProductsDetail;

public interface OrdersDao {
	public List<ProductsDetail> selectProductInforByCart(List<OrdersDetail> cart);
	public int getPriceForEachProductByProductIdAndColorId(int productId, int colorId);
}
