package org.pnv.mobileshop.dao.orders;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.pnv.mobileshop.model.OrdersDetail;
import org.pnv.mobileshop.model.ProductsDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional

public class OrdersDaoImplement implements OrdersDao {

	@Autowired
	private SessionFactory sessionFactory;

	public List<ProductsDetail> selectProductInforByCart(List<OrdersDetail> cart) {
		List<ProductsDetail> productsDetail = new ArrayList<>();
		
		List<Integer> indexs = new ArrayList<Integer>();
		for (int i = 0; i < cart.size(); i++) {
			indexs.add(cart.get(i).getProducts().getProducts_Id());
		}
		
		String SQL = "from ProductsDetail AS P WHERE P.products.products_Id IN (:ids)";

		Query query = sessionFactory.getCurrentSession().createQuery(SQL);
		query.setParameterList("ids", indexs);
		
		productsDetail =  (List<ProductsDetail>) query.list();
		
		return productsDetail;
	}

	// DONE
	public int getPriceForEachProductByProductIdAndColorId(int productId, int colorId) {
		String selectPrice = "from ProductsDetail WHERE products.products_Id = :pId AND color.color_Id = :cId";
		Query query = sessionFactory.getCurrentSession().createQuery(selectPrice);

		query.setParameter("pId", productId);
		query.setParameter("cId", colorId);
		ProductsDetail pd = (ProductsDetail) query.uniqueResult();

		return pd.getPrice();
	}

}