package org.pnv.mobileshop.model;

import java.util.Date;
import java.util.List;

/**
 * @author lai.le
 *
 */

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "products")
public class Products implements Serializable{
	
	// column in table
	@Id
    @GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private int products_Id;
	
	@Column(name = "ProductsName", length = 255)
	private String productsName;
	
	@Column(name = "Brand", length = 45)
	private String brand;
	
	@Column(name = "OperatingSystem", length = 45)
	private String operatingSystem;
	
	@Column(name = "FrontCamera", length = 45)
	private String frontCamera;
	
	@Column(name = "RearCamera", length = 45)
	private String rearCamera;
	
	@Column(name = "Rom", length = 45)
	private String rom;
	
	@Column(name = "Ram", length = 45)
	private String ram;
	
	@Column(name = "Screen", length = 255)
	private String screen;
	
	@Column(name = "DateOfManufacture")
	private Date dateOfManufature;
	
	@Column(name = "Battery", length = 255)
	private String battery;
	
	
	// mapping to product Detail --
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "products")
    private List<ProductsDetail> productsDetail;
	
	
	// mapping to comments
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "products")
    private List<Comments> comments;
	
	public Products() {
		super();
	}

	
	public Products(int id) {
		this.products_Id = id;
	}

	public Products(int products_Id, String productsName, String brand, String operatingSystem, String frontCamera,
			String rearCamera, String rom, String ram, String screen, Date dateOfManufature, String battery) {
		super();
		this.products_Id = products_Id;
		this.productsName = productsName;
		this.brand = brand;
		this.operatingSystem = operatingSystem;
		this.frontCamera = frontCamera;
		this.rearCamera = rearCamera;
		this.rom = rom;
		this.ram = ram;
		this.screen = screen;
		this.dateOfManufature = dateOfManufature;
		this.battery = battery;
	}
	
	
	// get and set
	public int getProducts_Id() {
		return products_Id;
	}

	public void setProducts_Id(int products_Id) {
		this.products_Id = products_Id;
	}

	public String getProductsName() {
		return productsName;
	}

	public void setProductsName(String productsName) {
		this.productsName = productsName;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public String getFrontCamera() {
		return frontCamera;
	}

	public void setFrontCamera(String frontCamera) {
		this.frontCamera = frontCamera;
	}

	public String getRearCamera() {
		return rearCamera;
	}

	public void setRearCamera(String rearCamera) {
		this.rearCamera = rearCamera;
	}

	public String getRom() {
		return rom;
	}

	public void setRom(String rom) {
		this.rom = rom;
	}

	public String getRam() {
		return ram;
	}

	public void setRam(String ram) {
		this.ram = ram;
	}

	public String getScreen() {
		return screen;
	}

	public void setScreen(String screen) {
		this.screen = screen;
	}

	public Date getDateOfManufature() {
		return dateOfManufature;
	}

	public void setDateOfManufature(Date dateOfManufature) {
		this.dateOfManufature = dateOfManufature;
	}

	public String getBattery() {
		return battery;
	}

	public void setBattery(String battery) {
		this.battery = battery;
	}

	public List<ProductsDetail> getProductsDetail() {
		return productsDetail;
	}

	public void setProductsDetail(List<ProductsDetail> productsDetail) {
		this.productsDetail = productsDetail;
	}

	public List<Comments> getComments() {
		return comments;
	}

	public void setComments(List<Comments> comments) {
		this.comments = comments;
	}
}
