package org.pnv.mobileshop.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "orders_detail")
public class OrdersDetail implements Serializable {

	// column in table
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private int ordersDetail_Id;

	// mapping to order --
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Orders_Id")
	@JsonIgnore
	private Orders orders;

	// mapping to product detail --
	@ManyToOne(targetEntity=ProductsDetail.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "Productsdetail_Id", referencedColumnName = "Id")
	@JsonIgnore
	private ProductsDetail productsDetail;

	@Column(name = "Quantity_Orders")
	private int quantity_Orders;

	// attribute in other class
	@OneToOne
	private ProductImages productsImage = new ProductImages();
	@OneToOne
	private Products products = new Products();
	@OneToOne
	private Color color = new Color();
	
	public OrdersDetail(int productId, int quantity_Order) {
		super();
		this.products.setProducts_Id(productId);
		this.quantity_Orders = quantity_Order;
	}

	public OrdersDetail(int productId, int quantity_Order, int colorId) {
		this.products.setProducts_Id(productId);
		this.quantity_Orders = quantity_Order;
		this.color.setColor_Id(colorId);
	}

	public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}

	public OrdersDetail() {
		super();
	}

	public ProductImages getProductsImage() {
		return productsImage;
	}

	public void setProductsImage(ProductImages productsImage) {
		this.productsImage = productsImage;
	}

	public ProductsDetail getProductsDetail() {
		return productsDetail;
	}

	public void setProductsDetail(ProductsDetail productsDetail) {
		this.productsDetail = productsDetail;
	}

	public int getOrdersDetail_Id() {
		return ordersDetail_Id;
	}

	public void setOrdersDetail_Id(int ordersDetail_Id) {
		this.ordersDetail_Id = ordersDetail_Id;
	}

	public int getQuantity_Order() {
		return quantity_Orders;
	}

	public void setQuantity_Order(int quantity_Order) {
		this.quantity_Orders = quantity_Order;
	}

	public int getQuantity_Orders() {
		return quantity_Orders;
	}

	public void setQuantity_Orders(int quantity_Orders) {
		this.quantity_Orders = quantity_Orders;
	}

	public Orders getOrders() {
		return orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
}
