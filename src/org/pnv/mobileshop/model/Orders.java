package org.pnv.mobileshop.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "orders")
public class Orders implements Serializable {

	// column in table
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private int orders_Id;

	// mapping to User --
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "User_Id", nullable = false)
	@JsonIgnore
	private User user;

	@Column(name = "ShippingAddress", length = 255)
	private String shippingAddress;

	@Column(name = "DateOrder")
	private Date dateOrder;

	// mapping to Order Detail --
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "orders")
	private List<OrdersDetail> ordersDetail;

	public Orders() {

	}

	public Orders(int orders_Id, String shippingAddress, Date dateOrder) {
		super();
		this.orders_Id = orders_Id;
		this.shippingAddress = shippingAddress;
		this.dateOrder = dateOrder;
	}

	public int getOrders_Id() {
		return orders_Id;
	}

	public void setOrders_Id(int orders_Id) {
		this.orders_Id = orders_Id;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public Date getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(Date dateOrder) {
		this.dateOrder = dateOrder;
	}

	public List<OrdersDetail> getOrdersDetail() {
		return ordersDetail;
	}

	public void setOrdersDetail(List<OrdersDetail> ordersDetail) {
		this.ordersDetail = ordersDetail;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}