package org.pnv.mobileshop.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "color")
public class Color implements Serializable {

	// column in table
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private int color_Id;

	@Column(name = "Color", length = 45)
	private String color;
	
	
	// mapping to products detail --
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "color")
	private List<ProductsDetail> productsDetail;

	public List<ProductsDetail> getProductsDetail() {
		return productsDetail;
	}

	public void setProductsDetail(List<ProductsDetail> productsDetail) {
		this.productsDetail = productsDetail;
	}

	public Color(int color_Id, String color) {
		super();
		this.color_Id = color_Id;
		this.color = color;
	}
	
	public Color(int color_Id){
		this.color_Id = color_Id;
	}

	public Color() {
		super();
	}

	public int getColor_Id() {
		return color_Id;
	}

	public void setColor_Id(int color_Id) {
		this.color_Id = color_Id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
