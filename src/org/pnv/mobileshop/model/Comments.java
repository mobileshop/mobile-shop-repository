package org.pnv.mobileshop.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "comments")
public class Comments implements Serializable {

	// column in table
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private int comments_Id;

	// mapping to product
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Products_Id", referencedColumnName = "Id", nullable = false)
	private Products products;

	// mapping User --
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "User_Id", referencedColumnName = "Id", nullable = false)
	private User user;

	@Column(name = "Comments", length = 255)
	private String comments;

	@Column(name = "CommentDay", nullable = false)
	private String commentDay;

	public Comments() {
	}

	public Comments(Products product, User user, String comments, String commentDay) {
		this.comments = comments;
		this.commentDay = commentDay;
		this.products = product;
		this.user = user;
	}

	public int getComments_Id() {
		return comments_Id;
	}

	public void setComments_Id(int comments_Id) {
		this.comments_Id = comments_Id;
	}
	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCommentDay() {
		return commentDay;
	}

	public void setCommentDay(String commentDay) {
		this.commentDay = commentDay;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}
}
