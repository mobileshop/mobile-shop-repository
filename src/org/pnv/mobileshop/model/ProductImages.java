package org.pnv.mobileshop.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "products_image")
public class ProductImages implements Serializable {

	// column in table --
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private int productsImage_ID;

	// mapping product detail --
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ProductsDetail_Id", referencedColumnName = "Id")
	private ProductsDetail productsDetail;

	@Column(name = "ImageUrl", length = 255)
	private String imageURL;

	// constructor
	public ProductImages() {
		super();
	}

	// get and set
	public int getProductsImage_ID() {
		return productsImage_ID;
	}

	public void setProductsImage_ID(int productsImage_ID) {
		this.productsImage_ID = productsImage_ID;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public ProductsDetail getProductsDetail() {
		return productsDetail;
	}

	public void setProductsDetail(ProductsDetail productsDetail) {
		this.productsDetail = productsDetail;
	}
}
