package org.pnv.mobileshop.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "productsdetail")
public class ProductsDetail implements Serializable {

	// column in table
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private int productsDetail_id;

	// mapping product --
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Products_Id", referencedColumnName = "Id", nullable = false)
	private Products products;

	// mapping color --
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Color_Id", referencedColumnName = "Id", nullable = false)
	@JsonIgnore
	private Color color;

	@Column(name = "Quantity")
	private int quantity;

	@Column(name = "Price")
	private int price;

	// mapping to order detail --
	@OneToMany(targetEntity=OrdersDetail.class,cascade = CascadeType.ALL,fetch = FetchType.LAZY, mappedBy = "productsDetail")
	private List<OrdersDetail> orderDetail;
	
	// mapping images --
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "productsDetail")
	private List<ProductImages> productImages;

	
	public List<OrdersDetail> getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(List<OrdersDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}

	@Column
    @ElementCollection(targetClass=String.class)
	private List<String> test;
	
	public ProductsDetail() {
		super();
	}
	
	public int getProductsDetail_id() {
		return productsDetail_id;
	}

	public void setProductsDetail_id(int productsDetail_id) {
		this.productsDetail_id = productsDetail_id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Products getProducts() {
		return products;
	}

	public void setProducts(Products products) {
		this.products = products;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}



	public List<ProductImages> getProductImages() {
		return productImages;
	}

	public void setProductImages(List<ProductImages> productImages) {
		this.productImages = productImages;
	}
}
