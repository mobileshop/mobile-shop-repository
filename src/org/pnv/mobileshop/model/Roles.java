package org.pnv.mobileshop.model;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Roles implements java.io.Serializable{
		
		// column in table
		@Id
		@GeneratedValue(strategy = IDENTITY)
	    @Column(name = "Id", unique = true, nullable = false)
		private int roles_Id;
		
	    @Column(name = "RolesName", nullable = false, length = 45)
		private String rolesName;
	    
	    // mapping to user
	    @OneToMany(fetch = FetchType.LAZY, mappedBy = "roles")
	    private List<User> user;
	    
	    public Roles() {
			super();
		}
	    
	    public Roles(int id){
	    	this.roles_Id = id;
	    }

		public int getRoles_Id() {
			return roles_Id;
		}

		public void setRoles_Id(int roles_Id) {
			this.roles_Id = roles_Id;
		}

		public String getRolesName() {
			return rolesName;
		}

		public void setRolesName(String rolesName) {
			this.rolesName = rolesName;
		}

		public List<User> getUser() {
			return user;
		}

		public void setUser(List<User> user) {
			this.user = user;
		}
		
}
