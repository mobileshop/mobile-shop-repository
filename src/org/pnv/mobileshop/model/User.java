package org.pnv.mobileshop.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "user")
public class User implements Serializable {

	// column in table
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private int user_Id;

	@Column(name = "UserName", unique = true, nullable = false, length = 255)
	private String userName;

	@Column(name = "Password", nullable = false, length = 255)
	private String passWord;

	@Column(name = "FullName", nullable = false, length = 45)
	private String fullName;

	@Column(name = "Address", nullable = false, length = 255)
	private String address;

	@Column(name = "PhoneNumber", nullable = false, length = 45)
	private String phoneNumber;

	// mapping to Order --
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Orders> orders;
	
	// mapping to roles
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Roles_Id", nullable = false)
	@JsonIgnore
	private Roles roles;

	// mapping to comments
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Comments> comments;

	
	public User() {

	}
	public User(int id) {
		this.user_Id = id;
	}
	

	public User(String userName, String passWord, String fullName, String address, String phoneNumber, Roles role) {
		super();
		this.userName = userName;
		this.passWord = passWord;
		this.fullName = fullName;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.roles = role;
	}

	public User(int user_Id,  String userName, String passWord, String fullName, String address,
			String phoneNumber) {
		super();
		this.user_Id = user_Id;
		this.userName = userName;
		this.passWord = passWord;
		this.fullName = fullName;
		this.address = address;
		this.phoneNumber = phoneNumber;
	}

	public int getUser_Id() {
		return user_Id;
	}

	public void setUser_Id(int user_Id) {
		this.user_Id = user_Id;
	}


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<Orders> getOrders() {
		return orders;
	}

	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}

	public Roles getRoles() {
		return roles;
	}

	public void setRoles(Roles roles) {
		this.roles = roles;
	}
	public List<Comments> getComments() {
		return comments;
	}

	public void setComments(List<Comments> comments) {
		this.comments = comments;
	}
}
