package org.pnv.mobileshop.business;

import java.util.List;

import org.pnv.mobileshop.model.OrdersDetail;
import org.pnv.mobileshop.model.ProductsDetail;

public interface OrdersBO {

	// THE SERVICE OF CART
	public List<OrdersDetail> addCartItem(List<OrdersDetail> cart, int productId, int quantity, int colorId);

	public List<OrdersDetail> updateCartItem(List<OrdersDetail> cart, int productId, int quantity);

	public List<OrdersDetail> deleteCartItem(List<OrdersDetail> cart, int productId);

	public int caculatorTotalPrice(List<OrdersDetail> cart);

	public List<ProductsDetail> showInfoProductsInCart(List<OrdersDetail> cart);

	public boolean productsHaveAlreadyExistInCart(int productId, List<OrdersDetail> cart);
	
	public int getQuantityByProductIdInCart(List<OrdersDetail> cart, int productId);
}
