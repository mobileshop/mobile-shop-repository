package org.pnv.mobileshop.business;

import org.pnv.mobileshop.model.*;

public interface UserBO {
	// login
	public User login(String userName, String passWord) throws UserNotFoundException;
	// register
	public User register(User user);
	// check user name
	public boolean accountIsExist(String userName);
	// take user by user_id
	public String boTakeFullNameById(int user_id);
}
