package org.pnv.mobileshop.business;

import java.util.List;


import org.pnv.mobileshop.dao.orders.OrdersDao;
import org.pnv.mobileshop.dao.orders.OrdersDaoImplement;
import org.pnv.mobileshop.model.OrdersDetail;
import org.pnv.mobileshop.model.ProductsDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OrdersBOImplement implements OrdersBO {
	
	@Autowired
	private OrdersDao ordersDao;

	
	@Override
	public List<OrdersDetail> deleteCartItem(List<OrdersDetail> cart, int productId) {
		for (int i = 0; i < cart.size(); i++) {
			if (productId == cart.get(i).getProducts().getProducts_Id()) {
				int index = i;
				cart.remove(index);
				break;
			}
		}
		return cart;
	}

	@Override
	public List<OrdersDetail> updateCartItem(List<OrdersDetail> cart, int productId, int quantity) {
		for( int i = 0 ; i < cart.size() ; i ++){
			if(cart.get(i).getProducts().getProducts_Id() == productId){
				cart.get(i).setQuantity_Order(quantity);
				break;
			}
		}
		return cart;
	}

	@Override
	public List<OrdersDetail> addCartItem(List<OrdersDetail> cart, int productId, int quantity, int colorId) {
		cart.add(new OrdersDetail(productId, quantity, colorId));
		return cart;
	}

	@Override
	public int caculatorTotalPrice(List<OrdersDetail> cart) {
		int totalPrice = 0;
		for (int i = 0; i < cart.size(); i++) {
			totalPrice = totalPrice + this.ordersDao.getPriceForEachProductByProductIdAndColorId(
					cart.get(i).getProducts().getProducts_Id(), cart.get(i).getColor().getColor_Id())
					* cart.get(i).getQuantity_Order();
		}
		return totalPrice;
	}

	@Override
	public List<ProductsDetail> showInfoProductsInCart(List<OrdersDetail> cart) {
		return this.ordersDao.selectProductInforByCart(cart);
	}

	@Override
	public boolean productsHaveAlreadyExistInCart(int productId, List<OrdersDetail> cart) {
		for (int i = 0; i < cart.size(); i++) {
			if (productId == cart.get(i).getProducts().getProducts_Id()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int getQuantityByProductIdInCart(List<OrdersDetail> cart, int productId) {
		int quantity = 0;
		for( int i = 0 ; i < cart.size() ; i ++){
			if(cart.get(i).getProducts().getProducts_Id() == productId){
				quantity = cart.get(i).getQuantity_Order();
				break;
			}
		}
		return quantity;
	}
}
