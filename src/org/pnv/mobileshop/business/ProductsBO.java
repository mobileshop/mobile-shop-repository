package org.pnv.mobileshop.business;

import java.util.*;

import org.pnv.mobileshop.model.*;

public interface ProductsBO {
	// select price, image and name of products
	public List<ProductsDetail> showProductsInformation();

	// show all product information
	public List<Products> showProductsDetailInformation();

	// show all product information by id
	public ProductsDetail showProductsDetailInformationById(int id);

	// show price, image and name of products by OperationSystem
	public List<ProductsDetail> showProductsDetailInformationByOperationSystem(String operationSystem);
	
	// show price, image and name of products by brand
		public List<ProductsDetail> showProductsDetailInformationByBrand(String brand);
	
	// show price, image and name of products by order ascending
	public List<ProductsDetail> showProductsDetailInformationByOrderAscending();
	
	// show price, image and name of products by order descending
	public List<ProductsDetail> showProductsDetailInformationByOrderDescending();
	
	// Show price, image and name of products by price 
	public List<ProductsDetail> showProductDetailInformationByPrice(String firstBoundary, String lastBoundary);
	
	public List<ProductsDetail> showColorForProductByProductId(int id);
	
	// search 
	public List<ProductsDetail> getDataBySearch(String key);
}
