package org.pnv.mobileshop.business;

import java.util.*;

import org.pnv.mobileshop.model.*;
public interface CommentBO {
	public List<Comments> getCommentForProducts(int Products_Id);
	public void addCommentForProducts(Comments comments);
}
