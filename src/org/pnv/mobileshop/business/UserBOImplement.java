package org.pnv.mobileshop.business;

import org.pnv.mobileshop.dao.usermanagement.UserDao;
import org.pnv.mobileshop.dao.usermanagement.UserDaoImplement;
import org.pnv.mobileshop.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class UserBOImplement implements UserBO {
	
	@Autowired
	private UserDao userDao;

	@Override
	public User login(String userName, String passWord) throws UserNotFoundException {
		User user = this.userDao.selectUserNameAndPasswordInUserTable(userName, passWord);
		if (user == null) {
			throw new UserNotFoundException();
		}
		return user;
	}

	@Override
	public User register(User us) {
		User user = this.userDao.insertNewDataIntoUserTable(us);
		return user;
	}

	@Override
	public boolean accountIsExist(String userName) {
		return this.userDao.selectUserNameFromUserTable(userName);
	}

	
	@Override
	public String boTakeFullNameById(int user_id){
		String fullName = this.userDao.selectFullNameByUserId(user_id);
		return fullName;
	}
	
	
}
