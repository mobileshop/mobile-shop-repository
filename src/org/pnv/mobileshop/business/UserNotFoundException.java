package org.pnv.mobileshop.business;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserNotFoundException extends Exception {
	
}
