package org.pnv.mobileshop.business;

import java.util.*;

import org.pnv.mobileshop.dao.productsmanagement.*;
import org.pnv.mobileshop.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductsBOImplement implements ProductsBO {
	
	@Autowired
	private ProductsDao productsDao;

	@Override
	public List<ProductsDetail> showProductsInformation() {
		return this.productsDao.selectPriceAndNameAndImageOfProduct();
	}

	@Override
	public List<Products> showProductsDetailInformation() {
		return this.productsDao.selectAllInformationOfProduct();
	}

	@Override
	public ProductsDetail showProductsDetailInformationById(int id) {
		return this.productsDao.selectAllInformationProductsById(id);
	}

	@Override
	public List<ProductsDetail> showProductsDetailInformationByOperationSystem(String operationSystem) {
		return this.productsDao.selectPriceAndNameAndImageOfProductsByOperationSystem(operationSystem);
	}

	@Override
	public List<ProductsDetail> showProductsDetailInformationByBrand(String brand) {
		return this.productsDao.selectPriceAndNameAndImageOfProductByBrand(brand);
	}

	@Override
	public List<ProductsDetail> showProductDetailInformationByPrice(String firstBoundary, String lastBoundary) {
		return this.productsDao.seclectPriceAndNameAndImageOfProductsByPrice(firstBoundary, lastBoundary);
	}

	@Override
	public List<ProductsDetail> showProductsDetailInformationByOrderAscending() {
		return this.productsDao.selectPriceAndNameAndImageOfProductsByOrderAscending();
	}

	@Override
	public List<ProductsDetail> showProductsDetailInformationByOrderDescending() {
		return this.productsDao.selectPriceAndNameAndImageOfProductsByOrderDescending();
	}

	@Override
	public List<ProductsDetail> showColorForProductByProductId(int id) {
		return this.productsDao.selectColorForProductByProductId(id);
	}

	@Override
	public List<ProductsDetail> getDataBySearch(String key) {
		return this.productsDao.seclectPriceAndNameAndImageOfProductsBySearch(key);
	}
}
