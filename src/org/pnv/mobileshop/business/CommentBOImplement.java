package org.pnv.mobileshop.business;


import java.util.List;
import org.pnv.mobileshop.model.Comments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.pnv.mobileshop.dao.comment.*;

@Service
@Transactional
public class CommentBOImplement implements CommentBO{
	@Autowired
	private CommentDao commentsDao;
	
	@Override
	public List<Comments> getCommentForProducts(int Products_Id) {
		List<Comments> comment = this.commentsDao.selectCommentFromCommentTableByProductId(Products_Id);
		return comment;
	}
	
	@Override
	public void addCommentForProducts(Comments comment) {
		this.commentsDao.insertCommentToCommentTable(comment);
	}
}
