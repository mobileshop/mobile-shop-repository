package org.pnv.mobileshop.controller;

import java.util.List;

import org.pnv.mobileshop.business.ProductsBO;
import org.pnv.mobileshop.dao.productsmanagement.Hibernate;
import org.pnv.mobileshop.dao.productsmanagement.ProductsDao;
import org.pnv.mobileshop.model.Products;
import org.pnv.mobileshop.model.ProductsDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	@Autowired
	private ProductsBO productsBO;
	
	@Autowired
	private Hibernate hibernate;
	
	public static final String HOMEPAGE = "home";
	
	// DONE
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String viewHomePage(ModelMap map) {
		List<ProductsDetail> products = productsBO.showProductsInformation(); 
		map.addAttribute("productsDetail", products);
		return HOMEPAGE;
	}
	// DONE
	@RequestMapping(value = "sortProductByOperatingSystem", method = RequestMethod.GET)
	public String viewProductsByOperatingSystem(ModelMap map,
			@RequestParam(value = "operatingSystem", required = true) String operating) {
		List<ProductsDetail> products = productsBO.showProductsDetailInformationByOperationSystem(operating);
		map.addAttribute("productsDetail", products);
		return HOMEPAGE;
	}
	
	
	//DONE
	@RequestMapping(value = "/sortProductByBrand", method = RequestMethod.GET)
	public String viewProductsByBrand(ModelMap map, @RequestParam(value = "brand", required = true) String brand) {
		List<ProductsDetail> products = productsBO.showProductsDetailInformationByBrand(brand);
		map.addAttribute("productsDetail", products);
		return HOMEPAGE;
	}

	//DONE
	@RequestMapping(value = "/sortProductByPrice", method = RequestMethod.GET)
	public String viewProductsByOrder(ModelMap map, @RequestParam(value = "type", required = true) String type) {
		if (type.equals("ascending")) {
			List<ProductsDetail> products = productsBO.showProductsDetailInformationByOrderAscending();
			map.addAttribute("productsDetail", products);
		} else if (type.equals("descending")) {
			List<ProductsDetail> products = productsBO.showProductsDetailInformationByOrderDescending();
			map.addAttribute("productsDetail", products);
		}
		return HOMEPAGE;
	}
	
	//DONE
	@RequestMapping(value = "/selectPrice", method = RequestMethod.GET)
	public String viewProductsByPrice(ModelMap map,
			@RequestParam(value = "firstPrice", required = true) String firstPrice,
			@RequestParam(value = "lastPrice", required = true) String lastPrice) {
		List<ProductsDetail> products = productsBO.showProductDetailInformationByPrice(firstPrice, lastPrice);
		map.addAttribute("productsDetail", products);
		return HOMEPAGE;
	}
}
