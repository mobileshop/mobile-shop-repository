package org.pnv.mobileshop.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.pnv.mobileshop.business.OrdersBO;
import org.pnv.mobileshop.business.ProductsBO;
import org.pnv.mobileshop.model.OrdersDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;

@Controller
public class CartController {

	public static final String CARTPAGE = "cart";
	
	@Autowired
	private ProductsBO productsBO;
	
	@Autowired
	private OrdersBO orderBO;
	
	private List<OrdersDetail> cart = new ArrayList<>();

	// DONE
	@RequestMapping(value = "/cart", method = RequestMethod.GET)
	public String viewCartPage(ModelMap map, HttpSession session, HttpServletRequest request,
			@RequestParam(value = "productId", required = true) String operating) {
		session = request.getSession();
		int productsId = Integer.parseInt(operating);
		
		if (orderBO.productsHaveAlreadyExistInCart(productsId, cart) == true) {
			cart = orderBO.updateCartItem(cart, productsId, orderBO.getQuantityByProductIdInCart(cart, productsId) + 1);
		} else {
			
			// show color first in list color
			int colorId = productsBO.showColorForProductByProductId(productsId).get(0).getColor().getColor_Id();
			// add product into cart
			cart = orderBO.addCartItem(cart, productsId, 1, colorId);
			// delete product in cart

			//
		}
		
		map.addAttribute("totalMoney", orderBO.caculatorTotalPrice(cart));
		session.setAttribute("cart", cart);
		session.setAttribute("productsInCart", orderBO.showInfoProductsInCart(cart));

		return CARTPAGE;
	}
	
	// DONE
	@RequestMapping(value = "/cart", method = RequestMethod.POST)
	public void editCart(ModelMap map, HttpSession session, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "action", required = true) String action) throws IOException {
		session = request.getSession();
		if (action.equals("changeQuantity")) {
			Map<String, Object> data = new HashMap<String, Object>();
			int numberOrder = Integer.parseInt(request.getParameter("numberOrder"));
			int productId = Integer.parseInt(request.getParameter("idProduct"));

			if (request.getParameter("type").equals("minus")) {
				if (numberOrder >= 2) {
					numberOrder = numberOrder - 1;
					cart = orderBO.updateCartItem(cart, productId, numberOrder);
				}
				data.put("ok", true);
				data.put("number", orderBO.getQuantityByProductIdInCart(cart, productId));
				data.put("result", orderBO.caculatorTotalPrice(cart));
			} else {
				numberOrder = numberOrder + 1;
				cart = orderBO.updateCartItem(cart, productId, numberOrder);
				data.put("ok", true);
				data.put("number", orderBO.getQuantityByProductIdInCart(cart, productId));
				data.put("result", orderBO.caculatorTotalPrice(cart));
			}
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(new Gson().toJson(data));
		}
	}
}
