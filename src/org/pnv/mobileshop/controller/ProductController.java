package org.pnv.mobileshop.controller;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.pnv.mobileshop.business.CommentBO;
import org.pnv.mobileshop.business.ProductsBO;
import org.pnv.mobileshop.model.Comments;
import org.pnv.mobileshop.model.Products;
import org.pnv.mobileshop.model.ProductsDetail;
import org.pnv.mobileshop.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProductController {
	
	@Autowired
	private ProductsBO productsBO;
	@Autowired
	private CommentBO commentBO;
	
	public static final String PRODUCTPAGE = "productDetail";
	
	// DONE
	@RequestMapping(value = "productDetail", method = RequestMethod.GET)
	public String viewProductDetail(ModelMap map, HttpServletRequest request, HttpServletResponse response,HttpSession session) {
		request.getSession();
		int productId = Integer.parseInt(request.getParameter("productId"));
		// INFOR A PRODUCT
		ProductsDetail productDetail = productsBO.showProductsDetailInformationById(productId);
		// COMMENTS FOR A PRODUCT
		List<Comments> comment = commentBO.getCommentForProducts(productId);
		
		map.addAttribute("productDetail", productDetail);
		map.addAttribute("commentForProduct", comment);
		map.addAttribute("productId", productId);
		
		return PRODUCTPAGE;
	}
	// DONE
	@RequestMapping(value = "productDetail", method = RequestMethod.POST)
	public String comment(ModelMap map, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		
		// get session from USER
		User user = (User) session.getAttribute("user");
		
		String userAction = request.getParameter("productId");
		String contentComment = request.getParameter("contentComment");
		int productId = Integer.parseInt(request.getParameter("productId"));
		
		// comment for product
			if (userAction.isEmpty() == false) {
				// ADD COMMENTS
				Date today;
				// convert day to string
				SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd");
				today = new Date(System.currentTimeMillis());
				
				Products pr = new Products(productId);
				User us = new User(user.getUser_Id());
				Comments cm = new Comments(pr,us,contentComment,timeFormat.format(today.getTime()));
				
				commentBO.addCommentForProducts(cm);
				
				ProductsDetail productDetail = productsBO.showProductsDetailInformationById(productId);
				List<Comments> comment = commentBO.getCommentForProducts(productId);

				map.addAttribute("productDetail", productDetail);
				map.addAttribute("commentForProduct", comment);
				map.addAttribute("productId", productId);
				return PRODUCTPAGE;
			}
		return null;
	}
}
