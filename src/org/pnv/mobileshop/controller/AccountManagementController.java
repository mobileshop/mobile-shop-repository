package org.pnv.mobileshop.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.pnv.mobileshop.business.UserBO;
import org.pnv.mobileshop.business.UserNotFoundException;
import org.pnv.mobileshop.model.Roles;
import org.pnv.mobileshop.model.User;
import org.pnv.mobileshop.util.SendMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;

@Controller
public class AccountManagementController {
	
	@Autowired ( required = true)
	private UserBO userBO;
	
	private SendMail sendmail = new SendMail();

	public static final String HOMEPAGE = "home";
	public static final String REGISTERPAGE = "register";
	
	// DONE
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registerPage() {
		return REGISTERPAGE;
	}
	
	// DONE
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String loginPage(ModelMap map, HttpSession session, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "userAction", required = true) String userAction) throws IOException {
		User user = new User();
		session = request.getSession();
		
		if (userAction.equals("login")) {
			String userName = request.getParameter("username");
			String passWord = request.getParameter("password");
			try {
				user = userBO.login(userName, passWord);
				// set session for user register
				session.setAttribute("user", user); 

				Map<String, Object> data = new HashMap<String, Object>();
				data.put("success", true);
				data.put("html",
						"<span style =color:green;>" + " Login Successfull - click Continue to Continue</span>");
				// Write response data as JSON.
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(new Gson().toJson(data));
			} catch (UserNotFoundException e) {
				Map<String, Object> data = new HashMap<String, Object>();
				data.put("success", false);
				data.put("html", "Your user name Or password was wrong - Please check and try again");
				// Write response data as JSON.
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(new Gson().toJson(data));
			}
		} else if (userAction.equals("checkUserName")) {
			Map<String, Object> data = new HashMap<String, Object>();
			String userName = request.getParameter("username");

			if (userName.length() <= 5) {
				data.put("lessThanFiveC", true);
				data.put("html", "Your username must be at least 6 characters !!");
			} else {
				data.put("lessThanFiveC", false);
				if (userBO.accountIsExist(userName) == true) {
					data.put("exist", true);
					data.put("html", "Your Username Was Exist - Please try again !!");
				} else {
					data.put("exist", false);
				}
			}
			// pass data to jsp
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(new Gson().toJson(data));

		} else if (userAction.equals("register")) {
			String userName = request.getParameter("username");
			String passWord = request.getParameter("password");
			String fullName = request.getParameter("fullname");
			String address = request.getParameter("address");
			String phoneNumber = request.getParameter("phonenumber");
			
			Roles newRoles = new Roles(2);
			
			User newUser = new User(userName, passWord, fullName, address, phoneNumber,newRoles);
			
			user = userBO.register(newUser);

			// set session for user register
			session.setAttribute("user", user);

			return HOMEPAGE;
		} else if (userAction.equals("sendMail")) {
			String userName = request.getParameter("email");
			String message = request.getParameter("message");

			sendmail.sendMail(userName, message);
			return HOMEPAGE;
		}
		return null;
	}
}
