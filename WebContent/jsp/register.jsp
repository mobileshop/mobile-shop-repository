<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Register</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel='stylesheet prefetch'
	href='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/css/materialize.min.css'>
<link rel='stylesheet prefetch'
	href='https://fonts.googleapis.com/icon?family=Material+Icons'>
<link rel="stylesheet" href="./resources/css/register/register.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$('#username').keyup(function() {
			var un = $("#username").val();
			$.ajax({
				type : "POST",
				url : './register?userAction=checkUserName',
				data : {
					username : un,
				},
				success : function(response) {
					if (response.lessThanFiveC == true) {
						$('#showMessageRegister').html(response.html);
						$('#showMessageRegister').css('display', 'block');
						$("#startRegister").attr("disabled", true);
					} else {
						if (response.exist == true) {
							$('#showMessageRegister').html(response.html);
							$('#showMessageRegister').css('display', 'block');
							$("#startRegister").attr("disabled", true);
						} else {
							$('#showMessageRegister').css('display', 'none');
							$("#startRegister").attr("disabled", false);
						}
					}
				},
			});
		});
	});
</script>

</head>

<body>
<body ng-controller="RegisterCtrl" ng-app="myApp">
	<div class="container">
		<div id="signup">
			<div class="signup-screen">
				<div class="space-bot text-center">
					<h1>Sign up</h1>
					<div class="divider"></div>
				</div>
				<form class="form-register" action="register" method="post"
					name="register" novalidate>
					<div class="input-field col s6">
						<input id="username" type="text" name="username"
							ng-model="username" required> <label class="name"
							for="username">Username *</label>
					</div>
					<p class="alert alert-success" id="showMessageRegister"
						style="display: none;"></p>

					<div class="input-field col s6">
						<input id="password" type="password" name="password"
							ng-model="password" ng-minlength='8' class="validate" required>
						<label for="password">Password</label>
					</div>
					<p class="alert alert-success"
						ng-show="form-register.password.$error.minlength || form.password.$invalid">Your
						password must be at least 8 characters.</p>

					<div class="input-field col s6">
						<input id="fullname" type="text" class="validate"
							name="fullname" required> <label for="fullname">Fullname *</label>
					</div>

					<div class="input-field col s6">
						<input id="address" type="text" name="address" class="validate"
							required> <label for="address">Address *</label>
					</div>

					<div class="input-field col s6">
						<input id="phonenumber" type="text" class="validate" name="phonenumber" required> <label for="phonenumber">Phonenumber
							*</label>
					</div>

					<div class="space-top text-center">
						<button id="startRegister" ng-disabled="form-register.$invalid"
							type="submit" class="waves-effect waves-light btn done">
							<i class="material-icons left">done</i> Register
						</button>
						<input type="hidden" name="userAction" value="register">
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
<script src='http://code.jquery.com/jquery-2.1.4.min.js'></script>
<script
	src='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.0/js/materialize.min.js'></script>
<script
	src='https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js'></script>

<script src="./resources/js/register/register.js"></script>

</body>
</html>


