<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="./resources/css/cart/cart.css">
<link href="./resources/css/bootstrap/bootstrap.css">
<link href="./resources/css/bootstrap/bootstrap.min.css">
<script type="text/javascript" src="./resources/js/cart/cart.js"></script>
<title>Giỏ Hàng</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>




<!--               This is Tuan's code, you should delete          -->


<!--  minus quantity -->

<script>
	function minusQuantity(idProduct) {
		$(document)
				.ready(
						function() {
							var numberOrder = $(
									"#" + idProduct + "-number-order").val();
							$
									.ajax({
										type : "POST",
										url : './cart?action=changeQuantity&type=minus',
										data : {
											numberOrder : numberOrder,
											idProduct : idProduct
										},
										success : function(response) {
											if (response.ok == true) {
												document.getElementById(""
														+ idProduct
														+ "-number-order").value = response.number;
												document
														.getElementById("total").value = response.result;
											}
										},
									});

						});
	}
</script>

<!-- add quantity -->
<script>
	function addQuantity(idProduct) {
		$(document)
				.ready(
						function() {
							var numberOrder = $(
									"#" + idProduct + "-number-order").val();
							$
									.ajax({
										type : "POST",
										url : './cart?action=changeQuantity&type=add',
										data : {
											numberOrder : numberOrder,
											idProduct : idProduct
										},
										success : function(response) {
											if (response.ok == true) {
												document.getElementById(""
														+ idProduct
														+ "-number-order").value = response.number;
												document
														.getElementById("total").value = response.result;
											}
										},
									});

						});
	}
</script>




</head>
<body>
	<%@include file="header2.jsp"%>
	<div class="content-cart">
		<div class="form-cart">
			<div class="product-cart">

				<div class="buy-more">
					<a href="<%=request.getContextPath()%>/"> Mua thêm sản phẩm khác </a>
				</div>
				<!--  LIST PRODUCT  -->

				<ul class="list-order">
					<c:forEach var="i" begin="0" end="${productsInCart.size() - 1}">
						<li class="order">
							<div class="order-img">
								<img width="100%" height="100%"
									src="<c:out value = "${productsInCart.get(i).productImages.get(0).imageURL}"></c:out>">
							</div>
							<div class="order-info">
								<strong class="name-product"
									style="color: #333; font-weight: bold;"><mark
										class="bg-success">
										<c:out
											value="${productsInCart.get(i).products.productsName}"></c:out>
									</mark></strong> <strong class="price-product"><input maxlength="8"
									size="8" type="text" readonly="readonly"
									style="text-align: center; border: 0px;"
									value="<c:out value="${productsInCart.get(i).price}"></c:out>">₫</strong>
								<br> <br>
								<div class="dropdown">
									<button class="btn btn-success dropdown-toggle" type="button"
										data-toggle="dropdown">
										Chọn Màu <span class="caret"></span>
									</button>
									<ul class="dropdown-menu">
										<li
											style="min-height: 0px; margin-top: 0px; padding-bottom: 0px; padding-top: 0px;"><a><c:out
													value="${productsInCart.get(i).color.color}"></c:out></a>
											<hr style="margin-top: 0px;">
									</ul>
								</div>
								<span class="button-delete"><button
										class="btn btn-info btn-sm" style="height: 28px;">
										<span class="glyphicon glyphicon-remove"></span> Xóa
									</button> </span>






								<button type="button" class="btn btn-danger"
									onclick="minusQuantity('${cart.get(i).getProducts().getProducts_Id()}')"
									style="float: right; height: 28px; line-height: 15px;">-</button>




								<input class="number-order" maxlength="1" size="1" type="text"
									size="1" readonly="readonly"
									id="${cart.get(i).getProducts().getProducts_Id()}-number-order"
									style="text-align: center"
									value='<c:out value="${cart.get(i).getQuantity_Order()}"></c:out>'>




								<button type="button" class="btn btn-success"
									onclick="addQuantity('${cart.get(i).getProducts().getProducts_Id()}')"
									style="float: right; height: 28px; line-height: 15px;">+</button>


							</div> <br>
							<hr
								style="width: 50%; color: black; height: 1px; margin-left: 25%">
						</li>
					</c:forEach>

				</ul>






				<!--  TOTAL MONEY -->

				<br>
				<div class="total-pay">
					<p>
						<span> <strong class="name-total-pay">Tổng tiền</strong> <strong
							class="price-total-pay"><input maxlength="10" size="10"
								type="text" readonly="readonly" id="total"
								style="text-align: center; border: 0px;"
								value="<c:out value="${totalMoney}"></c:out>"> ₫</strong>
						</span>
					</p>
				</div>
				<hr>

			</div>
			<div class="pay-cart">
				<div>
					<h1>Thông tin đơn hàng</h1>
				</div>
				<br>
				<div class="total-pay-left">
					<div class="dropdown1">
						<button class="dropbtn1">Chọn Thành Phố</button>
						<div class="dropdown-content1">
							<a href="#"> Tp. HCM </a> <a href="#"> Tp. Đà Nẵng </a> <a
								href="#"> Tp. Hà Nội </a>
						</div>
					</div>
					<br> <br> <input type="text" placeholder="Địa chỉ"
						class="pay-address">
				</div>

				<div class="total-pay-right">
					<div class="dropdown1">
						<button class="dropbtn1">Chọn Quận Huyện</button>
						<div class="dropdown-content1">
							<a href="#"> Quận 1 </a> <a href="#"> Quận 2 </a> <a href="#">
								Quận 3 </a>
						</div>
					</div>
					<br> <br> <input type="text" placeholder="Số điện thoại"
						class="pay-phonenumber">
				</div>
				<br> <br> <br>
				<div class="pay-bt-div">
					<br> <br> <br>
					<center>
						<button type="button" class="btn btn-warning" style="width: 35%;">
							<span class="glyphicon glyphicon-ok-circle"></span> Mua Ngay
						</button>
					</center>
				</div>
			</div>
		</div>
	</div>
	<%@include file="footer.jsp"%>
</body>
</html>