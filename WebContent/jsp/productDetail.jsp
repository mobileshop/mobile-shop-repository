<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css"
	href="./resources/css/productDetail.css">
<link rel="stylesheet" type="text/css"
	href="./resources/css/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css"
	href="./resources/css/bootstrap/bootstrap.min.css">
<script type="text/javascript"
	src="./resources/js/productDetail/productDetail.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>


<!-- ajax for login  -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
	$(document).ready(
			function() {
				$('#startLogin').click(
						function() {
							var un = $("#username").val();
							var pw = $("#password").val();
							$.ajax({
								type : "POST",
								url : './register?userAction=login',
								data : {
									username : un,
									password : pw
								},
								success : function(response) {
									if (response.success == false) {
										$('#showMessage')
												.html(response.html);
									} else {
										$('#showMessage').html(response.html);
										$('#showMessage').css('font-size',
												'20px', 'marin-left', '5%');
										$('#hideWhenLoginSuccess').css(
												'display', 'none');
										$('#showWhenLoginSuccess').css(
												'display', 'block');
									}
								},
							});
						});
			});
</script>






<title>Thông tin chi tiết</title>
</head>

<body>
	<%@include file="header2.jsp"%>
	<div class="productDetail">
		<div class="rowtop">
			<h3 style="margin-left: 100px;">
				Điện thoại
				<mark class="bg-danger"> <c:out
					value="${productDetail.products.productsName}"></c:out></mark>
			</h3>
		</div>
		<hr>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<img
						style="margin-left: -50px; max-width: 400px; max-height: 450px;"
						src="<c:out value="${productDetail.productImages.get(0).imageURL}"></c:out>">
					<div>
						<div class="row" style="margin-left: -20px;">
							<div class="col-xs-2">
								<a href="#"><img
									src="./images/productDetailPattern/iphone-7-plus-256gb-jet-black-180x125-1.png"></a>
								<center>
									<p>Jet Black</p>
								</center>
							</div>
							<div class="col-xs-2">
								<a href="#"><img
									src="./images/productDetailPattern/iphone-7-plus-256gb-mau-bac-180x125-1.png"></a>
								<center>
									<p>Bạc</p>
								</center>
							</div>
							<div class="col-xs-2">
								<a href="#"><img
									src="./images/productDetailPattern/iphone-7-plus-256gb-mau-den-180x125-1.png"></a>
								<center>
									<p>Đen</p>
								</center>
							</div>
							<div class="col-xs-2">
								<a href=""><img
									src="./images/productDetailPattern/iphone-7-plus-256gb-vang-1-180x125-hong.png"></a>
								<center>
									<p>Vàng hồng</p>
								</center>
							</div>
							<div class="col-xs-2">
								<a href="#"><img
									src="./images/productDetailPattern/iphone-7-plus-256gb-vang-dong-180x125-1.png"></a>
								<center>
									<p>Vàng đồng</p>
								</center>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div>
						<h3 class="text">
							Giá:
							<c:out value="${productDetail.price}"></c:out>
							<sup>đ</sup>
						</h3>
						<strong>Kiểm tra có hàng tại nơi bạn ở không?</strong>
					</div>

					<div class="dropdown">
						<br> <br>
						<button onclick="myFunction1()" class="dropbtn">Chọn
							thành phố</button>
						<div id="myDropdown1" class="dropdown-content">
							<a href="#">Tp. Hồ Chí Minh</a> <a href="#">Hà Nội</a> <a
								href="#">Đà Nẵng</a>
						</div>
					</div>

					<div class="dropdown">
						<button onclick="myFunction2()" class="dropbtn">Chọn quận
							huyện</button>
						<div id="myDropdown2" class="dropdown-content">
							<a href="#">Quận 1</a> <a href="#">Quận 2</a> <a href="#">Quận
								3</a>
						</div>
					</div>

					<div class="dropdown">
						<button onclick="myFunction3()" class="dropbtn">Chọn màu</button>
						<div id="myDropdown3" class="dropdown-content">
							<li><a href="#"><img
									src="./images/productDetailPattern/iphone-7-plus-256gb-vang-dong-180x125-1.png">
									Vàng đồng </a></li>
							<li><a href="#"><img
									src="./images/productDetailPattern/iphone-7-plus-256gb-mau-bac-180x125-1.png">
									Bạc </a></li>
							<li><a href="#"><img
									src="./images/productDetailPattern/iphone-7-plus-256gb-mau-den-180x125-1.png">
									Đen </a></li>
							<li><a href="#"><img
									src="./images/productDetailPattern/iphone-7-plus-256gb-vang-1-180x125-hong.png">
									Vàng hồng </a></li>
							<li><a href="#"><img
									src="./images/productDetailPattern/iphone-7-plus-256gb-jet-black-180x125-1.png">
									Jet black </a></li>
						</div>
					</div>

					<div class="button1">
						<button class="btn btn-danger">
							<a href="<%= request.getContextPath() %>/cart?productId=<c:out value="${productId}"></c:out>"&&>
								<center>
									<strong>ĐẶT HÀNG</strong><br>Xem hàng trực tuyến
								</center>
							</a>
						</button>
					</div>
					<div class="bottom_1">
						<center>
							Gọi đặt mua: <a>1800.1060</a> (Miễn phí)<br> <a>08.38.102.102</a>
							(7:30-22:00)
						</center>
					</div>
				</div>

				<div class="col-md-4"">
					<h3 class="text-info" style="margin-left: 28%; color: blue">Thông
						số kỹ thuật</h3>
					<br>
					<table class="table" style="margin-left: 5%;">
						<tbody>
							<tr class="success">
								<td>Tên Sản Phẩm</td>
								<td><c:out
										value="${productDetail.products.productsName}"></c:out>
								</td>
							</tr>
							<tr class="danger">
								<td>Hãng Sản Xuất</td>
								<td><c:out
										value="${productDetail.products.brand}"></c:out></td>
							</tr>
							<tr class="info">
								<td>Dung Lượng Pin</td>
								<td><c:out
										value="${productDetail.products.battery}"></c:out></td>
							</tr>
						</tbody>
					</table>
					<button type="button" class="button3" style="margin-left: 5%;"
						id="showDetail">Xem cấu hình chi tiết</button>
					<br>

					<!-- show when we click button -->

					<div class="modal fade" id="detailModel" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">
										<c:out
											value="${productDetail.products.productsName}"></c:out>
									</h4>
								</div>
								<table class="table">
									<tbody>
										<tr class="success">
											<td>Tên Sản Phẩm</td>
											<td><c:out
													value="${productDetail.products.productsName}"></c:out></td>
										</tr>
										<tr class="danger">
											<td>Hãng Sản Xuất</td>
											<td><c:out
													value="${productDetail.products.brand}"></c:out></td>
										</tr>
										<tr class="info">
											<td>Hệ Điều Hành</td>
											<td><c:out
													value="${productDetail.products.operatingSystem}"></c:out>
											</td>
										</tr>
										<tr class="warning">
											<td>Camera Trước</td>
											<td><c:out
													value="${productDetail.products.frontCamera}"></c:out></td>
										</tr>
										<tr class="active">
											<td>Camera Sau</td>
											<td><c:out
													value="${productDetail.products.rearCamera}"></c:out></td>
										</tr>
										<tr class="success">
											<td>Bộ Nhớ Trong</td>
											<td><c:out
													value="${productDetail.products.rom}"></c:out></td>
										</tr>
										<tr class="danger">
											<td>Ram</td>
											<td><c:out
													value="${productDetail.products.ram}"></c:out></td>
										</tr>
										<tr class="info">
											<td>Kích Thước Màn Hình</td>
											<td><c:out
													value="${productDetail.products.screen}"></c:out></td>
										</tr>
										<tr class="warning">
											<td>Dung Lượng Pin</td>
											<td><c:out
													value="${productDetail.products.battery}"></c:out></td>
										</tr>
										<tr class="active">
											<td>Ngày Sản Xuất</td>
											<td><c:out
													value="${productDetail.products.dateOfManufature}"></c:out></td>
										</tr>
									</tbody>
								</table>
								<div class="modal-footer">
									<button href="#" class="btn btn-info btn-sm"
										data-dismiss="modal">
										<span class="glyphicon glyphicon-remove-circle"></span> Close
									</button>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
		</div>
		<hr>
		<div class="container">
			<h3>
				Bình luận về
				<mark class="bg-success"> <c:out
					value="${productDetail.products.productsName}"></c:out></mark>
			</h3>
			<div class="row">
				<div class="col-md-8">
					<div class="row">
						<hr>
						<div class="text-comment">
							<form action="productDetail" method="post">
								<textarea name="contentComment" class="text-input-comment"
									id="text-input-comment" rows="3" cols="107"
									placeholder="Mời bạn để lại bình luận"></textarea>
								<br>
								<button <%if (session.getAttribute("user") != null) {%>
									type="submit" <%} else {%> type="button" id="login" <%}%>
									href="#" class="btn btn-info btn-sm"
									style="margin-left: 684px;">
									<span class="glyphicon glyphicon-comment"></span>Comment
								</button>
								<input type="hidden" name="productId"
									value="<%=request.getAttribute("productId")%>">
							</form>
						</div>
						<div class="list-comment">
							<c:forEach var="i" begin="0"
								end="${commentForProduct.size() - 1}">
								<div>
									<div class="icon-username">TT</div>
									<span class="user-comment"> <c:out
											value="${commentForProduct.get(i).user.fullName}"></c:out>
									</span> <br>
									<div class="comment">
										<c:out value="${commentForProduct.get(i).comments}"></c:out>
									</div>
									<p class="day-comment">
										<c:out value="${commentForProduct.get(i).commentDay}"></c:out>
									<p>
									<hr>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>

			<!-- form login dialog -->

			<div class="modal fade" id="loginModel" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header" style="padding: 35px 50px;">
							<h4 style="text-align: center">
								<span class="glyphicon glyphicon-lock"></span> Please Login
							</h4>

							<span id="showMessage" style="color: red; margin-left: 5%;"></span>

						</div>

						<!-- show when login success -->
						<div class="modal-body" style="padding: 40px 50px; display: none;"
							id="showWhenLoginSuccess">
							<form role="form" action="productDetail" method="POST">
								<button type="submit" class="btn btn-success btn-block"
									id="continue">
									<span class="glyphicon glyphicon-ok-circle"></span> Continue
								</button>
								<input type="hidden" name="productId"
									value="<%=request.getAttribute("productId")%>"> <input
									type="hidden" name="contentComment" id="newContentComment">
							</form>
						</div>

						<!-- show when chưa login -->
						<div class="modal-body" style="padding: 40px 50px;"
							id="hideWhenLoginSuccess">
							<form role="form">
								<div class="form-group">
									<label for="usrname"><span
										class="glyphicon glyphicon-user"></span> Username</label> <input
										type="text" class="form-control" id="username" name="username"
										placeholder="tuan handsome">
								</div>
								<div class="form-group">
									<label for="psw"><span
										class="glyphicon glyphicon-eye-open"></span> Password</label> <input
										type="password" class="form-control" id="password"
										name="password" placeholder="*****">
								</div>
								<div class="checkbox">
									<label><input type="checkbox" value="" checked>Remember
										me</label>
								</div>
								<button type="button" class="btn btn-success btn-block"
									id="startLogin">
									<span class="glyphicon glyphicon-off"></span> Login
								</button>
							</form>
						</div>
						<div class="modal-footer" style="background-color: #f9f9f9;">
							<button type="submit"
								class="btn btn-danger btn-default pull-left"
								data-dismiss="modal"
								style="font-size: 18px; width: 26%; margin-left: 6%;">
								<span class="glyphicon glyphicon-remove"></span> Cancel
							</button>
							<p>
								Not a member? <a href="<%=request.getContextPath()%>/register">Sign
									Up</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- script for login and show san pham -->
		<script>
			$(document).ready(function() {
				$("#showDetail").click(function() {
					$("#detailModel").modal();
				});
			});
		</script>

		<!--  hiện form login -->
		<script>
			$(document)
					.ready(
							function() {
								$("#login")
										.click(
												function() {
													var longTest = $("#text-input-comment");
													if (longTest.val().length < 16) {
														alert("Please enter comment more than 15 characters !!!");
													} else {
														$("#loginModel")
																.modal();
													}
												});
							});
		</script>

		<!--  pass data from one form to another form -->
		<script>
			$(document).ready(
					function() {
						$("#continue").click(
								function() {
									$('#newContentComment').val(
											$('#text-input-comment').val());
								});
					});
		</script>
		<%@include file="footer.jsp"%>
</body>
</html>

