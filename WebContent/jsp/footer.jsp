<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="./resources/css/footer.css">
</head>
<body>

	</div>


	<footer>
		<div class="container-footer">
			<div class="row">
				<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">
					<h3 class="footer-title">MEMBERS</h3>
					<div class="hyperlink">
						<a href="">Lê Văn Tuấn</a> <a href="">Nguyễn Văn Dũng</a> <a
							href="">Phạm Đình Sửu</a> <a href="">Lê Thị Hương Lài</a> <a
							href="">Phạm Hồng Phúc</a>
					</div>
				</div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">
					<h3 class="footer-title">VALUES OF TEAM</h3>
					<div class="hyperlink">
						<a href="">Respect</a> <a href="">Happiness</a> <a href="">Support</a>
						<a href="">Responsibility</a> <a href="">Solidarity</a> <a href="">Trust</a>
					</div>
				</div>
				<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 no-padding">
					<h3 class="footer-title">OTHERS</h3>
					<div class="hyperlink">
						<a href="">IDE and Tools</a> <a href="">Community of project</a> <a
							href="">Working Groups</a>
					</div>
				</div>
				<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding">
					<h3 class="footer-title">CONNECT WITH US</h3>
					<div class="social">
						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
							<a href=""><img src="./images/Footer/fb.png">Facebook</a> <a
								href=""><img src="./images/Footer/youtube.png">Youtube</a>
							<a href=""><img src="./images/Footer/tw.png">Twitter</a>
						</div>

						<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 no-padding">
							<a href=""><img src="./images/Footer/skype.png">Skype</a> <a
								href=""><img src="./images/Footer/in.png">LinkedIn</a> <a
								href=""><img src="./images/Footer/zalo.jpg">Zalo</a>
						</div>

					</div>



				</div>
				<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"
						style="margin-bottom: 50px; color: #636161; font-weight: bold; text-align: center;">
						<hr style="color: #636161">
						© Copyright 2016 AhihiShop.net, All rights reserved
					</div>


				</div>

			</div>
		</div>
	</footer>

</body>
</html>