<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Color</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>id</th>
		</tr>
		<%-- <c:forEach items="${productDetail}" var="productDetail">   --%>
		<tr>
			<td><c:out value="${productDetail.productsDetail_id}" /></td>
			<td><c:out value="${productDetail.products.productsName}" /></td>
			<td><c:out value="${productDetail.color.color}" /></td>
			<td><c:out value="${productDetail.quantity}" /></td>
			<td><c:out value="${productDetail.price}" /></td>
			</td>
			<c:out value="${productId}" />
			</td>
		</tr>
		<%--  </c:forEach>  --%>
	</table>

	<table>
		<tr>
			<th>Comments</th>
		</tr>
		<c:forEach items="${commentForProduct}" var="commentForProduct">
			<tr>
				<td><c:out value="${commentForProduct.comments}" /></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>