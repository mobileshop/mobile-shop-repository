<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./resources/css/header.css">
<link rel="stylesheet" type="text/css" href="./resources/css/search.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="./resources/css/message.css">




<script>
	$(document).ready(function() {
		$('#inputTextSearc').keyup(function() {
			var data = $("#inputTextSearch").val();
			$.ajax({
				type : "GET",
				url : './productDetail?action=search',
				data : {
					input : data,
				},
				success : function(response) {
					if (response.ok == true) {
						$('#resultSearchCss').css('display','block');
						$('#resultSearchCss').html(response.html);
					} else {
						$('#resultSearchCss').css('display','none');
					}
				},
			});
		});
	});
</script>




</head>
<body>
	<div class="container"
		style="background: #7d0874; padding: 10px 1px; width: 100%;">
		<nav>
			<ul class="mcd-menu">
				<li><img style="width: 300px; height: 100px;"
					src="./images/logoAhihi.png"></li>
				<li><a href="<%=request.getContextPath()%>/"> <i
						class="fa fa-home"></i> <strong>Trang Chủ</strong> <small>Ahihishop.com</small>
				</a></li>
				<li><a href=""> <i class="fa fa-mobile" aria-hidden="true"></i>
						<strong>Nhà sản xuất</strong> <small>Company make</small>
				</a>
					<ul>

						<li><a
							href="<%=request.getContextPath()%>/sortProductByOperatingSystem?operatingSystem=Ios"><i
								class="fa fa-apple" aria-hidden="true"></i>IOS</a></li>
						<li><a
							href="<%=request.getContextPath()%>/sortProductByOperatingSystem?operatingSystem=Android"><i
								class="fa fa-android" aria-hidden="true"></i>Android</a>
							<ul>
								<li><a
									href="<%=request.getContextPath()%>/sortProductByBrand?brand=Samsung"><i
										class="fa fa-android" aria-hidden="true"></i>Sam sung</a></li>
								<li><a
									href="<%=request.getContextPath()%>/sortProductByBrand?brand=Sony"><i
										class="fa fa-android" aria-hidden="true"></i>Sony</a></li>
								<li><a
									href="<%=request.getContextPath()%>/sortProductByBrand?brand=Oppo"><i
										class="fa fa-android" aria-hidden="true"></i>Oppo</a></li>
								<li><a
									href="<%=request.getContextPath()%>/sortProductByBrand?brand=Asus"><i
										class="fa fa-android" aria-hidden="true"></i>Asus</a></li>
								<li><a
									href="<%=request.getContextPath()%>/sortProductByBrand?brand=HTC"><i
										class="fa fa-android" aria-hidden="true"></i>HTC</a></li>
								<li><a
									href="<%=request.getContextPath()%>/sortProductByBrand?brand=LG"><i
										class="fa fa-android" aria-hidden="true"></i>LG</a></li>
								<li><a
									href="<%=request.getContextPath()%>/sortProductByBrand?brand=Lenovo"><i
										class="fa fa-android" aria-hidden="true"></i>Lenovo</a></li>
							</ul></li>

						<li><a
							href="<%=request.getContextPath()%>/sortProductByOperatingSystem?operatingSystem=Window"><i
								class="fa fa-windows" aria-hidden="true"></i>Window</a></li>
					</ul></li>
				<li><a href=""> <i class="fa fa-usd" aria-hidden="true"></i>
						<strong>Mức giá</strong> <small>Level cost</small>
				</a>
					<ul>
						<li><a
							href="<%=request.getContextPath()%>/selectPrice?firstPrice=0&lastPrice=1000000"><i
								class="fa fa-money" aria-hidden="true"></i>Nhỏ hơn 1 triệu</a></li>
						<li><a
							href="<%=request.getContextPath()%>/selectPrice?firstPrice=1000001&lastPrice=2000000"><i
								class="fa fa-money" aria-hidden="true"></i>Từ 1 đến 2 triệu</a></li>
						<li><a
							href="<%=request.getContextPath()%>/selectPrice?firstPrice=2000001&lastPrice=6000000"><i
								class="fa fa-money" aria-hidden="true"></i>Trên 2 đến 6 triệu</a></li>
						<li><a
							href="<%=request.getContextPath()%>/selectPrice?firstPrice=6000001&lastPrice=15000000"><i
								class="fa fa-money" aria-hidden="true"></i>Trên 6 đến 15 triệu</a></li>
						<li><a
							href="<%=request.getContextPath()%>/selectPrice?firstPrice=15000001&lastPrice=100000000"><i
								class="fa fa-money" aria-hidden="true"></i>Trên 15 triệu</a></li>
					</ul></li>

				<li><a href=""> <i class="fa fa-sort-alpha-desc"
						aria-hidden="true"></i> <strong>Sắp xếp</strong> <small>Sort</small>
				</a>
					<ul>
						<li><a
							href="<%=request.getContextPath()%>/sortProductByPrice?type=ascending"><i
								class="fa fa-long-arrow-up" aria-hidden="true"></i>Giá tăng dần</a></li>
						<li><a
							href="<%=request.getContextPath()%>/sortProductByPrice?type=descending"><i
								class="fa fa-long-arrow-down" aria-hidden="true"></i>Giá giảm
								dần </a></li>
					</ul></li>


				<li><a id="showMessageBox"> <i class="fa fa-comment-o"
						aria-hidden="true"></i> <strong>Liên hệ</strong> <small>Contact</small>
				</a></li>


				<!-- get session -->
				<li><a href=""> <i class="fa fa-shopping-cart"
						aria-hidden="true"></i> <strong>Giỏ hàng <sup
							style="color: red; font-weight: bold; font-size: 15px"><c:out
									value="${cart.size()}"></c:out></sup>
					</strong> <small>Cart</small>
				</a>
					<ul>
						<c:set var="size" value="${productsInCart.size()}" />
						<c:if test="${size > 0}">
							<c:forEach var="i" begin="0" end="${productsInCart.size()-1}">
								<li><a href="#"><i class="fa fa-cart-plus"
										aria-hidden="true"></i> <c:out
											value="${productsInCart.get(i).getProducts().getProductsName()}"></c:out>
										</mark></a></li>
							</c:forEach>
						</c:if>
					</ul></li>




				<li class="float"><a class="search"> <input type="text"
						value="" placeholder="what are you searching ?" id="inputTextSearch">
						<button>
							<i class="fa fa-search"></i>
						</button>
				</a> <a href="" class="search-mobile"> <i class="fa fa-search"></i>
				</a>
					
						
					</li>
			</ul>
			<div id="resultSearchCss" style="float: left; background-color: #f0f0f5; display: block; border-top: 4px solid #e67e22; margin-left: 82%; z-index: 10;width: 15%; height: auto;position: absolute;"></div>
		</nav>

		<script>
			$(document).ready(function() {
				$("#showMessageBox").click(function() {
					$("#messageBox").modal();
				});
			});
		</script>

		<script>
			
		</script>
	</div>


	<!--  search-->




	<!--  messageBox-->
	<div id="messageBox" class="modal">
		<!-- Modal content -->
		<div class="col-xm-6 col-sm-6 col-lg-6 modal-content">

			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>

			<center>
				<strong>Contact us</strong>
			</center>


			<form action="register" method="post">
				<div class="row" style="margin-top: 50px;">
					<div class="col-xm-6 col-sm-6 col-lg-6">
						<div class="input-group">
							<span class="input-group-addon"><i
								class="glyphicon glyphicon-user"></i></span> <input id="email"
								type="text" class="form-control" name="email"
								placeholder="Your name" style="width: 225%">
						</div>
					</div>
				</div>
				<div class="col-xm-12 col-sm-12 col-lg-12">

					<textarea class="col-xm-12 col-sm-12 col-lg-12"
						style="margin-top: 20px; margin-left: -15px; width: 105%; height: 100px;"
						name="message" placeholder="Your message"></textarea>
				</div>

				<div class="col-xm-12 col-sm-12 col-lg-12"
					style="margin-bottom: 30px;">
					<button type="submit" class="btn btn-default"
						style="float: right; margin-top: 20px;">
						Submit<span class="glyphicon glyphicon-send"
							style="margin-left: 10px;"></span>
					</button>
					<input type="hidden" name="userAction" value="sendMail">
				</div>
			</form>
		</div>

	</div>

</body>
</html>