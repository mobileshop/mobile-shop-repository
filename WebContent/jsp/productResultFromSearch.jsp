<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="./resources/css/search.css">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>



	<c:set var="size" value="${productsDetailResultSearch.size()} }"></c:set>
	<c:if test="${size >= 1}">
		<c:forEach var="i" begin="0"
			end="${productsDetailResultSearch.size() - 1}">
			<ul>
				<li><a href="#"> <span class="search-img"
						style="width: 50px; height: 50px; float: left; margin-right: 20px; marin-top: 10px;">
							<img
							src="${productsDetailResultSearch.get(i).getProductsImage().getImageURL()}">
					</span>
						<h4
							style="color: #333; font-size: 14px; font-weight: 700; float: left;">
							<c:out
								value="${productsDetailResultSearch.get(i).getProducts().getProductsName()}"></c:out>
						</h4> <span style="font-size: 12px; color: #c70100; float: none;"><c:out
								value="${productsDetailResultSearch.get(i).getPrice()"></c:out></span>
				</a></li>
				<hr>
			</ul>
		</c:forEach>
	</c:if>

</body>
</html>