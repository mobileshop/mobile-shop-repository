<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="./resources/css/header.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>

</head>
<body>
	<div class="container"
		style="background: #7d0874; padding: 10px 1px; width: 100%">
		<nav>
			<ul class="mcd-menu">
				<li><img style="width: 300px; height: 100px;"
					src="./images/logoAhihi.png"></li>
				<li><a href="<%=request.getContextPath()%>/"> <i
						class="fa fa-home"></i> <strong>Trang Chủ</strong> <small>Ahihishop.com</small>
				</a></li>

				<li><a href=""> <i class="fa fa-sign-in" aria-hidden="true"></i>
						<strong>Đăng nhập</strong> <small>Login</small>
				</a></li>

				<li><a href=""> <i class="fa fa-pencil-square-o"
						aria-hidden="true"></i> <strong>Đăng kí</strong> <small>Register</small>
				</a></li>

				<li><a href=""> <i class="fa fa-comment-o"
						aria-hidden="true"></i> <strong>Liên hệ</strong> <small>Contact</small>
				</a></li>

				<!-- get session -->
				<li><a href=""> <i class="fa fa-shopping-cart"
						aria-hidden="true"></i> <strong>Giỏ hàng <sup
							style="color: red; font-weight: bold; font-size: 15px"><c:out
									value="${cart.size()}"></c:out></sup>
					</strong> <small>Cart</small>
				</a>
					<ul>
						<c:set var="size" value="${productsInCart.size()}" />
						<c:if test="${size > 0}">
							<c:forEach var="i" begin="0" end="${productsInCart.size()-1}">
								<li><a href="#"><i class="fa fa-cart-plus"
										aria-hidden="true"></i> <c:out
											value="${productsInCart.get(i).getProducts().getProductsName()}"></c:out>
										</mark></a></li>
							</c:forEach>
						</c:if>
					</ul></li>

					
<!--     ------- -->
				<li class="float">
					<a class="search"> 
						<input type="text" value="" placeholder="search ..." onclick="myFunction()">
							<button> <i class="fa fa-search"></i> </button>
					</a> <a href="" class="search-mobile"> <i class="fa fa-search"></i> </a>

				    <ul>
				      	<li>
				      	<a href="#">
					       	<span class="search-img" style="background-color: white; width: 40px; height: 40px; float:left;  margin-right: 20px">
					       	<img src="./images/productDetailPattern/iphone-7-plus-256gb-mau-bac-180x125-1.png"
					        	style="width:40px; height:40px;">
					      	 </span>
					       	<span style="background-color: white; width: 30px; height: 40px;">
					        Sản phẩm 1
					        <b><small>1 500 000</small></b>
					       	</span>
				      	</a>
				      	</li>
   				 	</ul>
				</li>
<!--     -------- -->		
					

			</ul>
		</nav>
	</div>
</body>
</html>