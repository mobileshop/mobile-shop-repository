<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link rel="stylesheet" type="text/css" href=".\resources\css\home.css">

<!-- import library for slide -->
<link rel="stylesheet" type="text/css"
	href="./resources/css/slider/jquery.slider.css" />
<script type="text/javascript" src="./resources/js/slider/jquery.min.js"></script>
<script type="text/javascript"
	src="./resources/js/slider/jquery.slider.min.js"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



<!-- import icon font awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Import thư viện JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>
// kéo xuống khoảng cách 500px thì xuất hiện nút Top-up
var offset = 500;
// thời gian di trượt 0.75s ( 1000 = 1s )
var duration = 750;


$(function(){
$(window).scroll(function () {
if ($(this).scrollTop() > offset)
$('#top-up').fadeIn(duration);else
$('#top-up').fadeOut(duration);
});
$('#top-up').click(function () {
$('body,html').animate({scrollTop: 0}, duration);
});
});
</script>
<div title="Về đầu trang" onmouseover="this.style.color='#07560d'" onmouseout="this.style.color='#004993'" id="top-up">
<i class="fa fa-space-shuttle fa-rotate-270"></i></div>
<style>
#top-up {
background:none;
font-size: 3em;
text-shadow:0px 0px 5px #c0c0c0;
cursor: pointer;
position: fixed;
z-index: 9999;
color:#004993;
bottom: 20px;
right: 15px;
display: none;
}
</style>

</head>
<body>
	<%@include file="header.jsp"%>
	<div class="container">
		<div id="carousel-id" class="carousel slide" data-ride="carousel" >
		<ol class="carousel-indicators">
			<li data-target="#carousel-id" data-slide-to="0" class=""></li>
			<li data-target="#carousel-id" data-slide-to="1" class=""></li>
			<li data-target="#carousel-id" data-slide-to="2" class="active"></li>
		</ol>
		<div class="carousel-inner">
			<div class="item">
				<img data-src="holder.js/900x500/auto/#777:#7a7a7a/text:First slide" alt="First slide" src="https://cdn.fptshop.com.vn/Uploads/Originals/2016/12/28/636185531109466207_H1-iPhone7-Mua-iPhone-ch%C3%ADnh-h%C3%A3ng.jpg" width="100%" height="500">
				
			</div>
			<div class="item">
				<img data-src="holder.js/900x500/auto/#666:#6a6a6a/text:Second slide" alt="Second slide" src="https://fptshop.com.vn/Uploads/Originals/2017/1/10/636196041618135636_H1-F1s-OPPO.jpg" width="100%" height="500">
				
			</div>
			<div class="item active">
				<img data-src="holder.js/900x500/auto/#555:#5a5a5a/text:Third slide" alt="Third slide" src="https://cdn.fptshop.com.vn/Uploads/Originals/2016/12/28/636185370314562275_J-Series-H1.jpg" width="100%" height="50">
				<div class="container-fluid">
					<div class="carousel-caption">
						
					</div>
				</div>
			</div>
		</div>
		<a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
		<a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
	</div>
	<hr>
		<div id="productsDetail">
			<!-- get value for size of productsDetail -->
			<c:set var="size" value="${productsDetail.size()-1}" />
			
			<c:if test="${size >= 2}">
				<c:forEach var="i" begin="0" end="${productsDetail.size()-1}">
					<c:if test="${i % 5 == 0}">
						<div class="row-for-product">
							<c:forEach var="j" begin="${i}" end="${(i+5)-1}">
								<c:if test="${j - size  <= 0 }">
									<div class="cell-for-each-product">
									<a href="<%= request.getContextPath() %>/productDetail?productId=<c:out value="${productsDetail.get(j).productsDetail_id}"></c:out>">
										<div class="image-for-product">
											<img src="<c:out value="${productsDetail.get(j).productImages.get(0).imageURL}"></c:out>" />
										</div>
										<div class="name-for-product">
											<strong><c:out
													value="${productsDetail.get(j).products.productsName}"></c:out></strong>
										</div>
										<div class="price-for-product">
											<strong><c:out value="${productsDetail.get(j).price}"></c:out>₫</strong>
										</div>
									</a>	
									</div>
								</c:if>
							</c:forEach>
						</div>
					</c:if>
				</c:forEach>
			</c:if>
			<c:if test="${size < 2}">
				<div class="row-for-product">
					<div class="cell-for-each-product">
					<a href="<%= request.getContextPath() %>/productDetail?productId=<c:out value="${productsDetail.get(0).productsDetail_id}"></c:out>">
						<div class="image-for-product">
							<img src="<c:out value="${productsDetail.get(0).productImages.get(0).imageURL}"></c:out>" />
						</div>
						<div class="name-for-product">
							<strong><c:out
									value="${productsDetail.get(0).products.productsName}"></c:out></strong>
						</div>
						<div class="price-for-product">
							<strong><c:out value="${productsDetail.get(0).price}"></c:out>₫</strong>
						</div>
					</a>	
					</div>
					
				</div>
			</c:if>
		</div>
	</div>
<%@include file="footer.jsp"%>
</body>
</html>